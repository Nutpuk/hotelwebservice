package com.hotelservice

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import akka.actor.Actor
import models._
import Mongo.Hotels
import reactivemongo.bson.BSONDocument
import reactivemongo.core.commands.LastError
import spray.http.ContentTypes
import spray.http.HttpEntity
import spray.http.HttpResponse
import spray.http.StatusCodes
import spray.httpx.SprayJsonSupport.sprayJsonMarshaller
import spray.httpx.SprayJsonSupport.sprayJsonUnmarshaller
import spray.json.pimpAny
import spray.routing.HttpService

class MyServiceActor extends Actor with MyService {
  def actorRefFactory = context
  def receive = runRoute(myRoute)
}

trait MyService extends HttpService {

  lazy val myRoute =
    path("hotel") {
      get {
        getRoute
      }
    }


  protected lazy val getRoute =
      parameters('city.as[String]) { city ⇒
        detach() {
          complete {
            Hotels.findByCityID(city)
          }
        }
      } ~
      parameters('priceType.as[String]) { priceType ⇒
        detach() {
          complete {
            Hotels.findPriceByType(priceType)
          }
        }
      } ~
      detach() {
        complete {
          Hotels.findAll()
        }
      }
}
