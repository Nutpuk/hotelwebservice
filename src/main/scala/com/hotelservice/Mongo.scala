package com.hotelservice

import scala.concurrent.ExecutionContext
import scala.concurrent.ExecutionContext.Implicits.global
import reactivemongo.api.collections.default.BSONCollection
import reactivemongo.api.MongoDriver
import reactivemongo.bson.BSONDocument
import spray.json.RootJsonFormat
import sprest.models.UniqueSelector
import sprest.models.UUIDStringId
import sprest.reactivemongo.{BsonProtocol, ReactiveMongoPersistence, Sort}
import sprest.reactivemongo.typemappers.NormalizedIdTransformer
import sprest.reactivemongo.typemappers.SprayJsonTypeMapper

trait Mongo extends ReactiveMongoPersistence {
  import Akka.actorSystem

  private val driver = new MongoDriver(actorSystem)
  private val connection = driver.connection(List("localhost"))
  private val db = connection("hotelDB")

  implicit object JsonTypeMapper extends SprayJsonTypeMapper with NormalizedIdTransformer

  //Implement DAO
  abstract class UnsecuredDAO[M <: sprest.models.Model[String]](collName: String)(implicit jsformat: RootJsonFormat[M])
      extends CollectionDAO[M, String](db(collName)) {

    case class Selector(id: String) extends UniqueSelector[M, String]

    override def generateSelector(id: String) = Selector(id)
    override protected def addImpl(m: M)(implicit ec: ExecutionContext) = doAdd(m)
    override protected def updateImpl(m: M)(implicit ec: ExecutionContext) = doUpdate(m)
    override def remove(selector: Selector)(implicit ec: ExecutionContext) = uncheckedRemoveById(selector.id)

    protected val collection = db(collName)
    def removeAll()(implicit ec: ExecutionContext) = collection.remove(BSONDocument.empty)
    def findAll()(implicit ec: ExecutionContext) = find(BSONDocument.empty)

  }

  // MongoDB collections:
  import models._
  object Hotels extends UnsecuredDAO[Hotel]("hotel") with UUIDStringId {
    def findByCityID(city: String)(implicit ec: ExecutionContext) = find(BSONDocument("CITY" → city))
    def findPriceByType( priceType:String)(implicit ec: ExecutionContext) ={
      val sortType = if(priceType.toUpperCase == "DESC") Sort.Desc else Sort.Asc
      val data = find(BSONDocument.empty, new Sort("PRICE", sortType) )
      data
    }
  }
}
object Mongo extends Mongo
