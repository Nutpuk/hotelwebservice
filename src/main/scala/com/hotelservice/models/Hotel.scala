package com.hotelservice.models

import reactivemongo.bson.{BSONDocument, BSONDocumentReader, BSONObjectID}
import spray.json.DefaultJsonProtocol.jsonFormat3
import sprest.models.Model
import sprest.models.ModelCompanion
import sprest.reactivemongo.typemappers.jsObjectBSONDocumentWriter


case class Hotel(
                  ROOM: String,
                  HOTELID: Int,
                   CITY: String,
                   PRICE: Int,

                   var id: Option[String] = None) extends Model[String]

object Hotel extends ModelCompanion[Hotel, String] {
  import sprest.Formats._
  implicit val hotelJsonFormat = jsonFormat5(Hotel.apply _)
}


