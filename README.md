# Hotel Webservice #

## Dependencies ##
* Database : MongoDB
* Library: Reactive Mongo, Spray
* Language: Scala 

## Prerequisites ##
1.  Load dependencies
2. Import importDB/hoteldb.csv into mongoDB

```
#!cmd


mongoimport -d hotelDB -c hotel --type csv --file hoteldb.csv --headerline
```


## API ##

ip:port/hotel?parameter=value

in this example we use localhost(127.0.0.1) and port 8080

- Gel all hotel
http://127.0.0.1:8080/hotel

- Get hotel by city
http://127.0.0.1:8080/hotel?city=Bangkok

- Get price by DESC / ASC
http://127.0.0.1:8080/hotel?priceType=DESC